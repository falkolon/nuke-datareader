// Data reader example for Nuke

#include "DDImage/Reader.h"
#include "DDImage/Row.h"
#include "DDImage/ARRAY.h"
#include "DDImage/Thread.h"
#include "DDImage/Knob.h"
#include "DDImage/LUT.h"
#include <DDImage/Enumeration_KnobI.h>

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <ctime>

#include <chrono>
typedef std::chrono::high_resolution_clock Clock;

using namespace std;

// Test function that should do something with read file and signal whether it is something we can use
// Don't know what exactly to do here, so just return true
static bool test(int, const unsigned char* block, int length)
{
    return true;
}


class dataFormat : public DD::Image::ReaderFormat
{
public:
    dataFormat();

    void knobs(DD::Image::Knob_Callback c);
    void append(DD::Image::Hash& hash);

    bool _outputColorData;

private:

};

dataFormat::dataFormat()
{
    _outputColorData = false;
}


void dataFormat::knobs(DD::Image::Knob_Callback c)
{
    Bool_knob(c, &_outputColorData, "Output RGB values");
    SetFlags(c, DD::Image::Knob::STARTLINE);
}


void dataFormat::append(DD::Image::Hash& hash)
{

}


static DD::Image::ReaderFormat* buildformat(DD::Image::Read* iop)
{
    return new dataFormat();
}


class dataReader : public DD::Image::Reader
{
    FILE* in;

    DD::Image::MetaData::Bundle _meta;
    DD::Image::Channel channel[3];

    std::string _fileName;

    float* color;
    float* position;
    float* data;
    int width, height;
    int dataSize;

    DD::Image::Lock _lock;
    bool _firstTime;
    bool _fileRead;

    dataFormat* dFormat;
    bool* outputPositionValues;

    // How many data items make up a row
    int stride;
    // How many items in start position data
    int positionOffset;
    // How many items in starts color data
    int colorOffset;

public:
    const DD::Image::MetaData::Bundle& fetchMetaData(const char* key)
    {
        return _meta;
    }

    dataReader(DD::Image::Read*, int fd);
    ~dataReader();

    bool videosequence();
    void prefetchMetaData();
    void engine(int y, int x, int r, DD::Image::ChannelMask, DD::Image::Row &);
    void open();
    void append(DD::Image::Hash& hash);

    int getDataFileLength();
    int getDataFileLengthFast();
    void readDataFile();

    static const Description d;


};


dataReader::dataReader(DD::Image::Read* r, int fd) : DD::Image::Reader(r)
{
    // Cast reader format here to get access to knobs through format pointer
    dFormat = dynamic_cast<dataFormat*>(r->handler());

    data = nullptr;

    _firstTime = true;
    _fileRead = false;

    // Stride is the stride of data in rows, for XYZ it is 3, for XYZIRGB it is 7
    stride = 7;
    // Position offset is offset to first float for XYZ data within stride
    // It is usually zero
    positionOffset = 0;
    // Offset to color RGB data, for XYZIRGB data it is 4 (positions 0-3 are XYZI)
    colorOffset = 4;

    // Set default LUT to linear
    DD::Image::LUT* defaultLut;
    defaultLut = DD::Image::LUT::Linear();
    setLUT(defaultLut);

    channel[0] = DD::Image::Chan_Red;
    channel[1] = DD::Image::Chan_Green;
    channel[2] = DD::Image::Chan_Blue;

    // Find size of data in file and set image size relative to that by resizing one pixel larger
    dataSize = getDataFileLength();
    int dataSqrt = sqrt(dataSize);
    width = height = dataSqrt + 1;

    // Set the image format size
    set_info(width, height, 3);
}


int dataReader::getDataFileLength()
{
    auto t1 = Clock::now();
    
    // Open file for reading as a stream
    std::ifstream infile(filename());

    // Check for file accessibility
    if (!infile.good())
    {
        infile.close();
        return 0;
    }

    int numLines = 0;
    std::string line;
    while (std::getline(infile, line))
    {
        numLines++;
    }

    std::cout << "Number of lines in file: " << numLines << std::endl;

    // Close file
    infile.close();

    auto t2 = Clock::now();
    std::cout << "Delta time: "
              << std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count()
              << " ms" << std::endl;

    return numLines;
}


// Variant for finding number of lines by checking for line feed byte
// Not faster than ordinary version
int dataReader::getDataFileLengthFast()
{
    auto t1 = Clock::now();

    // Open stream from file for reading
    std::ifstream file(filename(), std::ios::binary | std::ios::ate);
    std::streamsize size = file.tellg();
    file.seekg(0, std::ios::beg);

    // Create new char array and add null terminator
    char* fileBuffer = new char[size + 1];
    fileBuffer[size] = '\0';

    int numLines = 0;

    char* end;

    if (file.read(fileBuffer, size))
    {
        // Initialize all pointers to start of file
        end = fileBuffer;

        // Iterate over file bytes until null terminator
        while (*end != '\0')
        {
            // In case of finding line end marking byte (endline or carriage return),
            // check for number of items in row and increment or decrement counters
            if (*end == '\n' || *end == '\r')
            {
                numLines++;
            }
            end++;
        }

        std::cout << "Number of lines fast: "  << numLines << std::endl;
    }

    // Release the char buffer and close file
    delete[] fileBuffer;
    file.close();

    auto t2 = Clock::now();
    std::cout << "Delta time: "
              << std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count()
              << " ms" << std::endl;

    return numLines;
}


// Main file reading routine
// This could probably be made faster by threading it and converting chars to floats in chunks in parallel
void dataReader::readDataFile()
{
    auto t1 = Clock::now();

    // Create array for data and initialize values
    int bufferSize = width * height * stride;
    delete data;
    data = new float[bufferSize];
    for (int x = 0; x < bufferSize; ++x)
    {
        data[x] = 0.0;
    }

    // Open stream from file for reading
    std::ifstream file(filename(), std::ios::binary | std::ios::ate);
    std::streamsize size = file.tellg();
    file.seekg(0, std::ios::beg);

    std::cout << "File size in bytes: " << size <<  std::endl;

    // Create new char array and add null terminator
    char* fileBuffer = new char[size + 1];
    fileBuffer[size] = '\0';

    int item_counter = 0;
    int counter = 0;
    int numLines = 0;

    char* begin;
    char* end;

    if (file.read(fileBuffer, size))
    {
        // Initialize all pointers to start of file
        end = begin = fileBuffer;

        // Iterate over file bytes until null terminator
        while (*end != '\0')
        {
            // In case of finding line end marking byte (endline or carriage return),
            // check for number of items in row and increment or decrement counters
            if (*end == '\n' || *end == '\r')
            {
                // In case of illegal row, meaning with less values,
                // trace back in data array and overwrite whatever was put there from this row
                if (item_counter < stride) {
                    counter -= item_counter;
                    numLines--;
                    if (counter < 0)
                        counter = 0;
                }
                item_counter = 0;
                numLines++;
                end++;
            }

            // strtof converts characters to float and updates end pointer to
            // point one char after last one in float
            begin = end;
            data[counter] = std::strtof( begin, &end );

            // Increment both processed float counter and item_counter
            // item_counter counts number of floats in a row
            counter++;
            item_counter++;
        }

        std::cout << "Number of floats processed: "  << counter - 1 << std::endl;
        std::cout << "Number of correct lines processed: "  << numLines << std::endl;
    }

    // Release the char buffer and close file
    delete[] fileBuffer;
    file.close();

    auto t2 = Clock::now();
    std::cout << "Delta time: "
              << std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count()
              << " ms" << std::endl;
}



void dataReader::open()
{
    _firstTime = true;
}


dataReader::~dataReader()
{
    delete data;
}


// Not sure how exactly to read sequence of arbitrary files, didn't get this working
// I don't know if videosequence has any relation to it
bool dataReader::videosequence()
{
    return true;
}


void dataReader::prefetchMetaData()
{

}


void dataReader::append(DD::Image::Hash& hash)
{

}


// The engine reads individual rows out of the input.
void dataReader::engine(int y, int x, int r, DD::Image::ChannelMask channels, DD::Image::Row& row)
{
    {
        DD::Image::Guard guard(_lock);
        if ( _firstTime && !_fileRead) {
            readDataFile();
            _firstTime = false;
            _fileRead = true;
        }

        if ( aborted() )
            return;
    }


    // Copying interleaved data based on stride and offsets
    for (int i = 3; i--; )
    {
        if (intersect(channels, channel[i]))
        {
            float* TO = row.writable(channel[i]) + x;
            float* END = TO + (r - x);
            int pxPtr = (width * y + x) * stride + i;
            while (TO < END)
            {
                // Check knob value from format for what to output
                if (dFormat && dFormat->_outputColorData)
                {
                    *TO++ = data[pxPtr + colorOffset];
                } else {
                    *TO++ = data[pxPtr + positionOffset];
                }
                pxPtr += stride;
            }
        }
    }
}


static DD::Image::Reader* build(DD::Image::Read* iop, int fd, const unsigned char* b, int n)
{
    return new dataReader(iop, fd);
}

const DD::Image::Reader::Description dataReader::d("data\0", build, test, buildformat);
